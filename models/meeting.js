'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Meeting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Meeting.init({
    room_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    day: DataTypes.DATE,
    scheduled_at: DataTypes.TIME
  }, {
    sequelize,
    modelName: 'Meeting',
  });

  Meeting.associate = models => {
    Meeting.belongsTo(models.User);
    Meeting.belongsTo(models.Room);
  };
  return Meeting;
};